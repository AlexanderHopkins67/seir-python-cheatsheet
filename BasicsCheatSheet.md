# Web Dev Basics Cheat Sheet

## Table Of Contents

### [Python](#python-1) -

- [Methods](#python---methods)

  - [String Methods](#string-methods)
  - [List Methods](#list-methods)
  - [Dictionary Methods](#dictionary-methods)

- [Functions](#python---functions)

- [Built-In Modules](#built-in-modules)
  - [Statistics](#statistics-module)
  - [Random](#random-module)
  - [Math](#math-module)

### [JavaScript](#javascript-1) -

- [Methods](#javascript---methods)
  - [Array Methods](#array-methods)
  - [Object Methods](#object-methods)
- [Functions](#javascript---functions)

- [Built-In Modules](#javascript---built-in-modules)

### [HTML](#html-1) -

- [Basic Structure Tags](#basic-structure-tags)
- [Formatting](#formatting)
- [Lists](#lists)
- [Text Tags](#text-tags)
- [Other Common Tags](#other-common-tags)


### [CSS](#css-basic-tags) -

- [Font Properties](#font-properties)
- [Font Text Properties](#font-text-properties)
- [Color Properties - Background Properties](#color-properties-background-properties)
- [Border Properties - Margin Properties](#border-properties-margin-properties)
- [Padding Properties](#padding-properties)
- [Border Properties](#border-properties)
- [Height, Width Properties](#height-width-properties)


### [Misc.](#misc-1) -

- [VSCode](#vscode)

  - [Shortcuts](#shortcuts)
  - [Theme & Style](#theme--styling)
  - [Extensions](#extensions)

- [Console Commands](#console-commands)
    - [Windows](#windows)
        - [Windows CLI Commands](#windows-cli-commands)
        - [Git](#git)
        - [Docker](#docker)
        - [Python](#python-2)
        - [JavaScript](#javascript-2)
    - [Mac](#mac)
        - [Mac CLI Commands](#mac-cli-commands)
        - [Git](#git-1)
        - [Docker](#docker-1)
        - [Python](#python-3)
        - [JavaScript](#javascript-3)

- [Tips & Tricks](#tips--tricks)

---

## Python

### [Python - Methods](https://docs.python.org/3/library/stdtypes.html#built-in-types):

**All Methods Are Called Using "Dot Notation" -> classInstance.method()**

#### [String Methods](<(https://docs.python.org/3/library/stdtypes.html#string-methods)>) -

| Method                                                                                | Description                                                                 |
| ------------------------------------------------------------------------------------- | --------------------------------------------------------------------------- |
| string[.capitalize()](https://docs.python.org/3/library/stdtypes.html#str.capitalize) | Converts the first character to upper case                                  |
| string[.count()](https://docs.python.org/3/library/stdtypes.html#str.count)           | Returns the number of times a specified value occurs in a string            |
| string[.endswith()](https://docs.python.org/3/library/stdtypes.html#str.endswith)     | Returns true if the string ends with the specified value                    |
| string[.isalnum()](https://docs.python.org/3/library/stdtypes.html#str.isalnum)       | Returns True if all characters in the string are alphanumeric               |
| string[.isalpha()](https://docs.python.org/3/library/stdtypes.html#str.isalpha)       | Returns True if all characters in the string are in the alphabet            |
| string[.isdigit()](https://docs.python.org/3/library/stdtypes.html#str.isdigit)       | Returns True if all characters in the string are digits                     |
| string[.islower()](https://docs.python.org/3/library/stdtypes.html#str.islower)       | Returns True if all characters in the string are lower case                 |
| string[.isnumeric()](https://docs.python.org/3/library/stdtypes.html#str.isnumeric)   | Returns True if all characters in the string are numeric                    |
| string[.isupper()](https://docs.python.org/3/library/stdtypes.html#str.isupper)       | Returns True if all characters in the string are upper case                 |
| string[.join()](https://docs.python.org/3/library/stdtypes.html#str.join)             | Converts the elements of an iterable into a string                          |
| string[.lower()](https://docs.python.org/3/library/stdtypes.html#str.lower)           | Converts a string into lower case                                           |
| string[.replace()](https://docs.python.org/3/library/stdtypes.html#str.replace)       | Returns a string where a specified value is replaced with a specified value |
| string[.split()](https://docs.python.org/3/library/stdtypes.html#str.split)           | Splits the string at the specified separator, and returns a list            |
| string[.strip()](https://docs.python.org/3/library/stdtypes.html#str.strip)           | Returns a trimmed version of the string                                     |
| string[.title()](https://docs.python.org/3/library/stdtypes.html#str.title)           | Converts the first character of each word to upper case                     |
| string[.upper()](https://docs.python.org/3/library/stdtypes.html#str.upper)           | Converts a string into upper case                                           |

#### [List Methods](https://docs.python.org/3/library/stdtypes.html#sequence-types-list-tuple-range) -

| Method                                                                                     | Description                                                     |
| ------------------------------------------------------------------------------------------ | --------------------------------------------------------------- |
| list[.append()](https://docs.python.org/3/library/stdtypes.html#mutable-sequence-types)    | Adds an element at the end of the list                          |
| list[.clear()](https://docs.python.org/3/library/stdtypes.html#mutable-sequence-types)     | Removes all the elements from the list                          |
| list[.copy()](https://docs.python.org/3/library/stdtypes.html#mutable-sequence-types)      | Returns a copy of the list                                      |
| list[.count()](https://docs.python.org/3/library/stdtypes.html#common-sequence-operations) | Returns the number of elements with the specified value         |
| list[.index()](https://docs.python.org/3/library/stdtypes.html#common-sequence-operations) | Returns the index of the first element with the specified value |
| list[.insert()](https://docs.python.org/3/library/stdtypes.html#mutable-sequence-types)    | Adds an element at the specified position                       |
| list[.pop()](https://docs.python.org/3/library/stdtypes.html#mutable-sequence-types)       | Removes the element at the specified position                   |
| list[.remove()](https://docs.python.org/3/library/stdtypes.html#mutable-sequence-types)    | Removes the first item with the specified value                 |
| list[.reverse()](https://docs.python.org/3/library/stdtypes.html#mutable-sequence-types)   | Reverses the order of the list                                  |
| list[.sort()](https://docs.python.org/3/library/stdtypes.html#list.sort)                   | Sorts the list                                                  |

#### [Dictionary Methods](https://docs.python.org/3/library/stdtypes.html#mapping-types-dict) -

| Method                                                                       | Description                                               |
| ---------------------------------------------------------------------------- | --------------------------------------------------------- |
| dict[.clear()](https://docs.python.org/3/library/stdtypes.html#dict.clear)   | Removes all the elements from the dictionary              |
| dict[.copy()](https://docs.python.org/3/library/stdtypes.html#dict.copy)     | Returns a copy of the dictionary                          |
| dict[.get()](https://docs.python.org/3/library/stdtypes.html#dict.get)       | Returns the value of the specified key                    |
| dict[.items()](https://docs.python.org/3/library/stdtypes.html#dict.items)   | Returns a list containing a tuple for each key value pair |
| dict[.keys()](https://docs.python.org/3/library/stdtypes.html#dict.keys)     | Returns a list containing the dictionary's keys           |
| dict[.pop()](https://docs.python.org/3/library/stdtypes.html#dict.pop)       | Removes the element with the specified key                |
| dict[.values()](https://docs.python.org/3/library/stdtypes.html#dict.values) | Returns a list of all the values in the dictionary        |

### [Python - Functions](https://docs.python.org/3/library/functions.html?):

| Function                                                                      | Description                                                                     |
| ----------------------------------------------------------------------------- | ------------------------------------------------------------------------------- |
| [abs()](https://docs.python.org/3/library/functions.html?highlight=abs#abs)   | Returns the absolute value of a number                                          |
| [bool()](https://docs.python.org/3/library/functions.html?highlight=abs#bool) | Returns the boolean value of the specified object                               |
| [dict()](https://docs.python.org/3/library/functions.html?#func-dict)         | Returns a dictionary (Array)                                                    |
| [enumerate()](https://docs.python.org/3/library/functions.html?#enumerate)    | Takes a collection (e.g. a tuple) and returns it as an enumerate object         |
| [filter()](https://docs.python.org/3/library/functions.html?#filter)          | Use a filter function to exclude items in an iterable object                    |
| [float()](https://docs.python.org/3/library/functions.html?#float)            | Returns a floating point number                                                 |
| [input()](https://docs.python.org/3/library/functions.html?#input)            | Allowing user input                                                             |
| [int()](https://docs.python.org/3/library/functions.html?#int)                | Returns an integer number                                                       |
| [isinstance()](https://docs.python.org/3/library/functions.html?#isinstance)  | Returns True if a specified object is an instance of a specified object         |
| [issubclass()](https://docs.python.org/3/library/functions.html?#issubclass)  | Returns True if a specified class is a subclass of a specified object           |
| [len()](https://docs.python.org/3/library/functions.html?#len)                | Returns the length of an object                                                 |
| [list()](https://docs.python.org/3/library/functions.html?#func-list)         | Returns a list                                                                  |
| [map()](https://docs.python.org/3/library/functions.html?#map)                | Returns the specified iterator with the specified function applied to each item |
| [max()](https://docs.python.org/3/library/functions.html?#max)                | Returns the largest item in an iterable                                         |
| [min()](https://docs.python.org/3/library/functions.html?#min)                | Returns the smallest item in an iterable                                        |
| [pow()](https://docs.python.org/3/library/functions.html?#pow)                | Returns the value of x to the power of y                                        |
| [print()](https://docs.python.org/3/library/functions.html?#print)            | Prints to the standard output device                                            |
| [range()](https://docs.python.org/3/library/functions.html?#func-range)       | Returns a sequence of numbers, starting from 0 and increments by 1 (by default) |
| [reversed()](https://docs.python.org/3/library/functions.html?#reversed)      | Returns a reversed iterator                                                     |
| [round()](https://docs.python.org/3/library/functions.html?#round)            | Rounds a numbers                                                                |
| [set()](https://docs.python.org/3/library/functions.html?#func-set)           | Returns a new set object                                                        |
| [slice()](https://docs.python.org/3/library/functions.html?#slice)            | Returns a slice object                                                          |
| [sorted()](https://docs.python.org/3/library/functions.html?#sorted)          | Returns a sorted list                                                           |
| [str()](https://docs.python.org/3/library/functions.html?#func-str)           | Returns a string object                                                         |
| [sum()](https://docs.python.org/3/library/functions.html?#sum)                | Sums the items of an iterator                                                   |
| [tuple()](https://docs.python.org/3/library/functions.html?#func-tuple)       | Returns a tuple                                                                 |
| [type()](https://docs.python.org/3/library/functions.html?#type)              | Returns the type of an object                                                   |
| [zip()](https://docs.python.org/3/library/functions.html?#zip)                | Returns an iterator, from two or more iterators                                 |

### [Python - Built-In Modules](https://docs.python.org/3/py-modindex.html):

#### [Statistics Module](https://docs.python.org/3/library/statistics.html#module-statistics):

```python
import statistics
```

| Method                                                                                     | Description                                                 |
| ------------------------------------------------------------------------------------------ | ----------------------------------------------------------- |
| statistics[.mean()](https://docs.python.org/3/library/statistics.html#statistics.mean)     | Calculates the mean (average) of a list of numbers          |
| statistics[.median()](https://docs.python.org/3/library/statistics.html#statistics.median) | Calculates the median (middle value) of a list of numbers   |
| statistics[.mode()](https://docs.python.org/3/library/statistics.html#statistics.mode)     | Calculates the mode (central tendency) of a list of numbers |

#### [Random Module](https://docs.python.org/3/library/random.html#module-random):

```python
import random
```

| Method                                                                           | Description                                                    |
| -------------------------------------------------------------------------------- | -------------------------------------------------------------- |
| random[.seed()](https://docs.python.org/3/library/random.html#random.seed)       | Initialize the random number generator                         |
| random[.randint()](https://docs.python.org/3/library/random.html#random.randint) | Returns a random number between the given range                |
| random[.choice()](https://docs.python.org/3/library/random.html#random.choice)   | Returns a random element from the given sequence               |
| random[.choices()](https://docs.python.org/3/library/random.html#random.choices) | Returns a list with a random selection from the given sequence |
| random[.shuffle()](https://docs.python.org/3/library/random.html#random.shuffle) | Takes a sequence and returns the sequence in a random order    |
| random[.random()](https://docs.python.org/3/library/random.html#random.random)   | Returns a random float number between 0 and 1                  |

#### [Math Module](https://docs.python.org/3/library/math.html#module-math):

```python
import math
```

| Method                                                                         | Description                                                                                 |
| ------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------- |
| math[.acos()](https://docs.python.org/3/library/math.html#math.acos)           | Returns the arc cosine of a number in radians                                               |
| math[.asin()](https://docs.python.org/3/library/math.html#math.asin)           | Returns the arc sine of a number in radians                                                 |
| math[.atan()](https://docs.python.org/3/library/math.html#math.atan)           | Returns the arc tangent of a number in radians                                              |
| math[.ceil()](https://docs.python.org/3/library/math.html#math.ceil)           | Rounds a number up to the nearest integer                                                   |
| math[.comb()](https://docs.python.org/3/library/math.html#math.comb)           | Returns the number of ways to choose k items from n items without repetition and order      |
| math[.cos()](https://docs.python.org/3/library/math.html#math.cos)             | Returns the cosine of a number                                                              |
| math[.degrees()](https://docs.python.org/3/library/math.html#math.degrees)     | Converts an angle from radians to degrees                                                   |
| math[.factorial()](https://docs.python.org/3/library/math.html#math.factorial) | Returns the factorial of a number                                                           |
| math[.floor()](https://docs.python.org/3/library/math.html#math.floor)         | Rounds a number down to the nearest integer                                                 |
| math[.gcd()](https://docs.python.org/3/library/math.html#math.gcd)             | Returns the greatest common divisor of two integers                                         |
| math[.isclose()](https://docs.python.org/3/library/math.html#math.isclose)     | Checks whether two values are close to each other, or not                                   |
| math[.perm()](https://docs.python.org/3/library/math.html#math.perm)           | Returns the number of ways to choose k items from n items with order and without repetition |
| math[.pow()](https://docs.python.org/3/library/math.html#math.pow)             | Returns the value of x to the power of y                                                    |
| math[.prod()](https://docs.python.org/3/library/math.html#math.pro)            | Returns the product of all the elements in an iterable                                      |
| math[.radians()](https://docs.python.org/3/library/math.html#math.radians)     | Converts a degree value into radians                                                        |
| math[.sin()](https://docs.python.org/3/library/math.html#math.sin)             | Returns the sine of a number                                                                |
| math[.sqrt()](https://docs.python.org/3/library/math.html#math.sqrt)           | Returns the square root of a number                                                         |
| math[.tan()](https://docs.python.org/3/library/math.html#math.tan)             | Returns the tangent of a number                                                             |
| math[.trunc()](https://docs.python.org/3/library/math.html#math.trunc)         | Returns the truncated integer parts of a number                                             |

## JavaScript

### [JavaScript - Methods]():

**All Methods Are Called Using "Dot Notation" -> classInstance.method()**

#### [String Methods](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) -

| Method                                                                                                                      | Description                                                                                                                                                   |
| --------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| String[.concat()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/concat)           | Combines the text of two (or more) strings and returns a new string.                                                                                          |
| String[.includes()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes)       | determines whether the original string contains the search string.                                                                                            |
| String[.indexOf()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/indexOf)         | Returns the index within the calling String object of the first occurrence of searchValue, or -1 if not found.                                                |
| String[.replace()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace)         | Used to replace occurrences of searchFor using replaceWith. searchFor may be a string or Regular Expression, and replaceWith may be a string or function.     |
| String[.replaceAll()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replaceAll)   | Used to replace all occurrences of searchFor using replaceWith. searchFor may be a string or Regular Expression, and replaceWith may be a string or function. |
| String[.slice()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/slice)             | Extracts a section of a string and returns a new string.                                                                                                      |
| String[.split()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split)             | Returns an array of strings populated by splitting the calling string at occurrences of the substring sep.                                                    |
| String[.toLowerCase()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toLowerCase) | Returns the calling string value converted to lowercase.                                                                                                      |
| String[.length](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/length)             | Returns the length of the string                                                                                                                              |
| String[.toUpperCase()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toUpperCase) | Returns the calling string value converted to uppercase.                                                                                                      |

#### [Array Methods](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#instance_methods) -

| Method                                                                                                              | Description                                                                                                               |
| ------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| Array[.filter()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)     | Returns a new array containing all elements of the original array for which the provided filtering function returns true. |
| Array[.forEach()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach)   | Calls a function for each element in the calling array.                                                                   |
| Array[.includes()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes) | Determines whether the calling array contains a value, returning true or false as appropriate.                            |
| Array[.indexOf()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf)   | Returns the first (least) index at which a given element can be found in the calling array.                               |
| Array[.join()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join)         | Joins all elements of an array into a string.                                                                             |
| Array[.map()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)           | Returns a new array containing the results of invoking a function on every element in the calling array.                  |
| Array[.pop()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/pop)           | Removes the last element from an array and returns that element.                                                          |
| Array[.push()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push)         | Adds one or more elements to the end of an array, and returns the new length of the array.                                |
| Array[.reverse()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse)   | Reverses the order of the elements of an array in place. (First becomes the last, last becomes first.)                    |
| Array[.shift()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/shift)       | Removes the first element from an array and returns that element.                                                         |
| Array[.slice()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice)       | Extracts a section of the calling array and returns a new array.                                                          |
| Array[.sort()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)         | Sorts the elements of an array in place and returns the array.                                                            |
| Array[.splice()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)     | Adds and/or removes elements from an array.                                                                               |

#### [Object Methods](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) -

| Method                                                                                                                      | Description                                                                                                         |
| --------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| Object[.assign()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)           | Copies the values of all enumerable own properties from one or more source objects to a target object.              |
| Object[.entries()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/entries)         | Returns an array containing all of the [key, value] pairs of a given object's own enumerable string properties.     |
| Object[.fromEntries()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/fromEntries) | eturns a new object from an iterable of [key, value] pairs. (This is the reverse of Object.entries).                |
| Object[.keys()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys)               | Returns an array containing the names of all of the given object's own enumerable string properties.                |
| Object[.valueOf()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/valueOf)         | Returns the primitive value of the specified object.                                                                |
| Object[.values()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/values)           | Returns an array containing the values that correspond to all of a given object's own enumerable string properties. |

### [JavaScript - Functions]():

| Function | Description |
| -------- | ----------- |

### [JavaScript - Built-In Modules]():

#### [--- Module]():

```JavaScript
import blank from "blank"
```

| Method | Description |
| ------ | ----------- |

## HTML

### [Basic Structure Tags](https://stuyhsdesign.wordpress.com/basic-html/structure-html-document/):
| Tag | Description |
| --- | ----------- |
| Shortcut[!] | Auto-populates your HTML document with the five basic HTML tags |
| Element[!DOCTYPE html] | Defines the document type |
| Tag[html] | Defines the root of an HTML document |
| Tag[head] | Contains metadata/information for the document |
| Tag[title] | Defines a title for the document |
| Tag[body] | Defines the document's body |

### [Formatting](https://web.stanford.edu/group/csp/cs21/htmlcheatsheet.pdf):
| Tag | Description |
| --- | ----------- |
| Tag[p] | Defines a paragraph |
| Element[br] | Defines a single line break |
| Tag[blockquote] | Puts content in a quote - indents text from both sides |
| Tag[div] | Used to format block content |
| Tag[span] | Used to format inline content |

### [Lists](https://web.stanford.edu/group/csp/cs21/htmlcheatsheet.pdf):
| Tag | Description |
| --- | ----------- |
| Tag[ul] | Creates an unordered list |
| Tag[ol] | Creates an ordered list |
| Tag[li] | Defines a list item |
| Tag[dl] | Defines a description list |
| Element[dt] | Defines a term/name in a description list |
| Element[dd] | Defines a description/value of a term in a description list |

### [Text Tags](https://web.stanford.edu/group/csp/cs21/htmlcheatsheet.pdf):
| Tag | Description |
| --- | ----------- |
| Tag[h1 - h6] | Defines HTML headings |
| Tag[b] | Defines bold text |
| Tag[em] | Defines emphasized text |
| Tag[strong] | Defines important text, commonly will be bolded |
| Tag[i] | Defines italic text |
| Tag[u] | Defines underlined text |



### [Other Common Tags](https://www.semrush.com/blog/html-tags-list/):
| Tag | Description |
| --- | ----------- |
| Tag[img] | Allows you to input an image |
| Tag[a] | Anchor tag for a link |
| Tag[address] | Defines the contact information for the author/owner of a document |
| Tag[abbr] | Defines an abbreviation |
| Tag[q] | Defines an inline quotation |

## CSS

### [CSS Basic Tags](https://www.w3schools.com/css/)

#### [Font Properties](https://www.w3schools.com/css/css_font.asp)
| Property | Description |
| --- | ----------- |
| Font | Used to combine all properties of fonts. EXAMPLE: P { font: italic bold 12pt/14pt Times, serif; } |
| Font-Family | Changes the font family of certain words, sentences, paragraphs, etc. EXAMPLE: P { font-family: "New Century Schoolbook", Times, serif; } |
| Font-Style | Changes test: normal, oblique, and italics. EXAMPLE: H1 { font-style: oblique; } P { font-style: normal; } |
| Font-Size | Used to modify the size of the displayed font. EXAMPLE: H1 { font-size: large; } or P { font-size: 12pt; } LI { font-size:90% } STRONG { font-size:larger; } |

#### [Font Text Properties](https://www.w3schools.com/css/css_text.asp)
| Property | Description |
| --- | ----------- |
| Text-Align | Used to justify text left, center, right, and justify. EXAMPLE: H1 { text-align: center; } |
| Text-Indent | Used to specify the amount of indentation prior to the first line of text. EXAMPLE: P { text-indent: 5em; } |
| Text-Decoration | Allows text to be decorated through one of five properties: underline, overline, line-through, blink, none. EXAMPLE: A:link, A:visited, A:active { text-decorationL none; } |
| Vertical-Align | Used to alter the vertical positioning of an inline element, relative to its parent element or to the element's line. EXAMPLE: IMG.middle { vertical-align: middle; } IMG { vertical-align: 50%; } |

#### [Color Properties](https://www.w3schools.com/css/css_colors.asp) - [Background Properties](https://www.w3schools.com/css/css_background.asp)
| Property | Description |
| --- | ----------- |
| Color | Changes the color of the text. EXAMPLE: H1 { color:blue; } or H2 { color: #000080; } |
| Background | Used to combine all properties of background. EXAMPLE: Body { background: white url(https://www.example.com/example.gif); } P { background: #7fffd4; } |
| Background-Color | Sets the background color of an element. EXAMPLE: BODY { background-color: white; }  H1 { background-color: 000080; } |
| Background-Image | Sets the background image of an element. EXAMPLE: BODY { background-image: url(/images/example.gif); } P { background-image: url(https://www.exampleimage.com/img.png); } |
| Background-Repeat | Determines how a specified background image is repeated. The repeat-x value will repeat the image horizontally while the repeat-y value will repeat the image vertically. EXAMPLE: BODY { background: white url(example.gif); background-attachment: fixed; } |

#### [Border Properties](https://www.w3schools.com/css/css_border.asp) - [Margin Properties](https://www.w3schools.com/css/css_margin.asp)
| Property | Description |
| --- | ----------- |
| Margin | Sets the margins of an element by specifying top, bottom, left and right margins -- all either specifying length or percentage. EXAMPLE: BODY { margin: 5pt; } /* all margins 5pt */ P { margin: 50% 30%; } /* top & bottom 50%, left & right 30% */ DIV { margin: 1em, 2em, 3em, 4em; } /* top margin 1em, right margin 2em, bottom margin 3em, left margin 4em */ |
| Margin-Top | Sets the top margin of an element by specifying a length or a percentage. EXAMPLE: BODY { margin-top: 5pt; } |
| Margin-Right | Sets the right margin of an element by specifying a length or a percentage. EXAMPLE: P { margin-top: 50%; } |
| Margin-Bottom | Sets the bottom margin of an element by specifying a length or a percentage. EXAMPLE: DIV { margin-top: 10pt; } |
| Margin-Left | Sets the left margin of an element by specifying a length or a percentage. EXAMPLE: H1 { margin-top: 30%; } |

#### [Padding Properties](https://www.w3schools.com/css/css_padding.asp)
| Property | Description |
| --- | ----------- |
| Padding | Shorthand for the padding-top, padding-right, padding-bottom, and padding-left properties. EXAMPLE: BLOCKQUOTE { padding: 2em 4em 6em 2em; } /* padding-top 2em, padding-right 4em, padding-bottom 6em, padding-left 2em */ |
| Padding-Top | Describes the amount of space between the top border and the content of the selector. EXAMPLE: P { Padding-Top: 20%; } |
| Padding-Right | Describes the amount of space between the right border and the content of the selector. EXAMPLE: P { Padding-Right: 20px; } |
| Padding-Bottom | Describes the amount of space between the bottom border and the content of the selector. EXAMPLE: P { Padding-Bottom: 20em; } |
| Padding-Left | Describes the amount of space between the left border and the content of the selector. EXAMPLE: P { Padding-Left: 20pt; } |

#### [Border Properties](https://www.w3schools.com/css/css_border.asp)
| Property | Description |
| --- | ----------- |
| Border | Sets the width, style, and/or color of an element's border. EXAMPLE: P { border: 10px, red, double;} /* You can individualize each parameter as needed. You can just use P { border: 10px; } if you are wanting to just create more border for the element. |
| Border-Top | Sets the width, style and/or color of an element's top border. EXAMPLE: P { border-top: 10px, red, double; } or P { border-top: 10px; } |
| Border-Right | Sets the width, style and/or color of an element's right border. EXAMPLE: P { border-right: 10px, red, double; } or P { border-right: 10px; } |
| Border-Bottom | Sets the width, style and/or color of an element's bottom border. EXAMPLE: P { border-bottom: 10px, red, double; } or P { border-bottom: 10px; } |
| Border-Left | Sets the width, style and/or color of an element's left border. EXAMPLE: P { border-left: 10px, red, double; } or P { border-left: 10px; } |
| Border-Color | Used to set the color of an element's border. EXAMPLE: P {border-color: #00000; } or P {border-color: white;} |
| Border-Width | Used to set the width of an element's border ( either all borders, or specifying top border. right border, bottom border, left border). EXAMPLE: P { border-width: 20%; } or P { border-width: 10px 5px 10px 5px } |
| Border-Left-Width | Used to specify the width of an element's left border. EXAMPLE: P { border-left: 20%; } |

### [Height, Width Properties](https://www.w3schools.com/css/css_dimension.asp)
| Property | Description |
| --- | ----------- |
| Height | Each block-level or replaced element can be given a height, specified as a length percentage or as auto. EXAMPLE: P { height: 15px; } H1 { height: 35%; } DIV { height: auto; } |
| Width | Each block-level or replaced element can be given a width, specified as a length percentage or as auto. EXAMPLE: P { width: 15px; } H1 { width: 35%; } DIV { width: auto; } |

## Misc.

### VSCode -

#### Shortcuts:

| Shortcut | Description |
| -------- | ----------- |

#### Theme & Styling:

#### Extensions:

## Console Commands:

### Windows

#### [Windows CLI Commands](https://www.w3schools.com/whatis/whatis_cli.asp)
| Command | Description |
| ------ | ----------- |
| ls or dir | List the directory (folder) system. |
| cd PATHNAME | Change directory (folder) in the file system, whatever you input for pathname is the folder you are navigating to. |
| cd\ | Move to the root folder of the file system. |
| cd.. | Move one level up (one folder) in the system. |
| mkdir FOLDERNAME or md FOLDERNAME | Creates a new directory (folder), whatever you input in FOLDERNAME will be the name of the new folder once entered. |
| cls | Clears the CLI window. |
| exit | Closes the CLI window. |
| code . | Opens the folder in VSC - Virtual Studio Code. |

### Mac

#### [Mac CLI Commands](https://www.w3schools.com/whatis/whatis_cli.asp)
| Command | Description |
| ------ | ----------- |
| ls | List the directory (folder) system. |
| cd PATHNAME | Change directory (folder) in the file system, whatever you input for pathname is the folder you are navigating to. |
| cd. | Displays the current directory. |
| cd.. | Move one level up (one folder) in the file system. |
| mkdir FOLDERNAME | Creates a new directory (folder), whatever you input in FOLDERNAME will be the name of the new folder once entered. |
| clear | Clears the CLI window. |
| exit | Closes the CLI window. |
| code. | Opens the folder in VSC - Virtual Studio Code. |


#### Git

| Command | Description |
| ------- | ----------- |

#### Docker

| Command | Description |
| ------- | ----------- |

#### Python

| Command | Description |
| ------- | ----------- |

#### JavaScript

| Command | Description |
| ------- | ----------- |

#### Mac

#### Git

| Command | Description |
| ------- | ----------- |

#### Docker

| Command | Description |
| ------- | ----------- |

#### Python

| Command | Description |
| ------- | ----------- |

#### JavaScript

| Command | Description |
| ------- | ----------- |

## Virtual Environments

| Command                      | Description                                                              |
| ---------------------------- | ------------------------------------------------------------------------ |
| python -m venv ./.venv       | Creates a virtual environment in a hidden local subdirectory named .venv |
| ls -a                        | Lists all files within a directory, including those hidden               |
| .\.venv\Scripts\Activate.ps1 | Activates the virtual environment on WINDOWS with Powershell             |
| source ./.venv/bin/activate  | Activates the virtual environment on macOS                               |
| deactivate                   | Deactivates the virtual environment                                      |

## Django

### Starting a Django Project from Scratch

| Command                                                                   | Description                                                                                                 |
| ------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| python -m pip install --upgrade pip                                       | Upgrades pip to the latest version                                                                          |
| pip install django                                                        | Installs Django within your virtual environment                                                             |
| deactivate                                                                | Deactivates the virtual environment to ensure project dependencies are captured accurately in the next step |
| pip freeze > requirements.txt                                             | Records project's dependencies within requirements.txt                                                      |
| django-admin startproject <project_name> .                                | Starts a project with name <project_name> in the present working directory                                  |
| python manage.py startapp <app_name>                                      | Starts an app with name <app_name> in the present working directory                                         |
| INSTALLED_APPS = ["<app_name>.apps.<config_class_name_within_apps_file>"] | Enables Django project to recognize the presence of the app                                                 |
| python manage.py migrate                                                  | Establishes the initial database table, including default admin and auth models                             |
| python manage.py runserver                                                | Runs the Django server at http://localhost:8000 by default                                                  |

### Tips & Tricks -
